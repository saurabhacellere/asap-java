package com.atlassian.asap.service.api;

import com.atlassian.asap.service.api.TokenValidator.Policy;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation that declares the intent for a resource to use ASAP-based authentication and authorization.
 * <p>
 * The annotation's parameters correspond to the corresponding methods that are documented for {@link TokenValidator};
 * see that class for more detail about their meanings.  All values default to the same behavior that would be
 * seen if the corresponding method is not called on the {@code TokenValidator}.
 * </p>
 *
 * @since 2.8
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PACKAGE, ElementType.TYPE, ElementType.METHOD})
public @interface AsapAuth {
    /**
     * The issuers that are authorized to execute as per {@link TokenValidator#issuer(String...)}.
     */
    String[] issuer() default {};

    /**
     * The issuers that are authorized impersonate other users, as per {@link TokenValidator#impersonationIssuer(String...)}.
     */
    String[] impersonationIssuer() default {};

    /**
     * Whether or not to allow subject impersonation, as per {@link TokenValidator#subjectImpersonation(boolean)}.
     *
     * @deprecated as per {@link TokenValidator#subjectImpersonation(boolean)}
     */
    @Deprecated
    boolean subjectImpersonation() default false;

    /**
     * The subjects that are authorized, as per {@link TokenValidator#subject(String...)}.
     */
    String[] subject() default {};

    /**
     * Explicit audience values to allow, as per {@link TokenValidator#audience(String...)}.
     */
    String[] audience() default {};

    /**
     * The enforcement policy to be applied, as per {@link TokenValidator#policy(Policy)}.
     */
    Policy policy() default Policy.REQUIRE;
}
