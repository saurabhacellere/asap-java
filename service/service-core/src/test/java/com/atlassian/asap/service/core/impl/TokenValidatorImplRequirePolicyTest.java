package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.service.api.ValidationResult.Decision;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static com.atlassian.asap.service.api.TokenValidator.Policy.REQUIRE;
import static java.util.Optional.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TokenValidatorImplRequirePolicyTest extends AbstractTokenValidatorImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void noTokenReturnsNotAuthenticated() {
        tokenValidator.policy(REQUIRE);

        assertThat(tokenValidator.validate(empty()), result(Decision.NOT_AUTHENTICATED));
    }

    @Test
    public void wrongPrefixReturnsNotAuthenticated() throws Exception {
        tokenValidator.policy(REQUIRE);

        assertThat(tokenValidator.validate(Optional.of("Not a jwt")), result(Decision.NOT_AUTHENTICATED));

        verifyZeroInteractions(jwtParser);
    }

    @Test
    public void invalidTokenReturnsNotAuthenticated() throws Exception {
        tokenValidator.policy(REQUIRE);
        when(jwtValidator.readAndValidate(TOKEN)).thenThrow(new MissingRequiredClaimException(JwtClaims.RegisteredClaim.ISSUER));

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHENTICATED));
    }

    @Test
    public void unresolvableKeyReturnsNotVerified() throws Exception {
        tokenValidator.policy(REQUIRE);
        when(jwtValidator.readAndValidate(TOKEN)).thenThrow(new CannotRetrieveKeyException("Ouch!"));

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_VERIFIED, ISSUER));
    }

    @Test
    public void badIssuerReturnsNotAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE).issuer(AUDIENCE1);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void badEffectiveSubjectReturnsNotAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE).subject(AUDIENCE1);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void goodEffectiveSubjectReturnsAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE).subject(ISSUER);
        final Jwt jwt = jwtBuilder.build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void subjectImpersonationRequiresAnIssuerWhitelist() throws Exception {
        tokenValidator.policy(REQUIRE)
                .subject(ISSUER)
                .subjectImpersonation(true);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        thrown.expect(IllegalStateException.class);
        tokenValidator.validate(HEADER);
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void noEffectiveSubjectWhenImpersonationIsEnabled() throws Exception {
        tokenValidator.policy(REQUIRE)
                .issuer(ISSUER)
                .subject(ISSUER)
                .subjectImpersonation(true);
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwtBuilder.build());

        assertThat(tokenValidator.validate(HEADER), result(Decision.NOT_AUTHORIZED, ISSUER));
    }

    @Test
    public void goodSubjectReturnsAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE).subject(ISSUER);
        final Jwt jwt = jwtBuilder.subject(Optional.of(ISSUER)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void goodSubjectWithImpersonationReturnsAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE)
                .issuer(ISSUER)
                .subject(AUDIENCE1)
                .subjectImpersonation(true);
        final Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void validImpersonationReturnsAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE)
                .issuer(ISSUER2)
                .impersonationIssuer(ISSUER);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void validImpersonationReturnsAuthorizedIssuerIsEmpty() throws Exception {
        tokenValidator.policy(REQUIRE)
                .impersonationIssuer(ISSUER);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void bothIssuerAndImpersonationIssuerAreEmpty() throws Exception {
        tokenValidator.policy(REQUIRE);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }

    @Test
    public void impersonationReturnsAuthorized() throws Exception {
        tokenValidator.policy(REQUIRE)
                .issuer(ISSUER)
                .impersonationIssuer(ISSUER2);
        Jwt jwt = jwtBuilder.subject(Optional.of(AUDIENCE1)).build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }
}
