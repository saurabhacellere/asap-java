package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.service.api.ValidationResult.Decision;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.asap.service.api.TokenValidator.Policy.REQUIRE;
import static java.util.Optional.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TokenValidatorImplTest extends AbstractTokenValidatorImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void requireIsTheDefaultPolicy() {
        assertThat(tokenValidator.validate(empty()), result(Decision.NOT_AUTHENTICATED));
    }

    @Test
    public void globalAudienceIsImplicitlyIncluded() throws Exception {
        tokenValidator.policy(REQUIRE).subject(ISSUER).audience();
        Jwt jwt = jwtBuilder.build();
        when(jwtValidator.readAndValidate(TOKEN)).thenReturn(jwt);

        assertThat(tokenValidator.validate(HEADER), result(Decision.AUTHORIZED, jwt));

        when(config.audience()).thenReturn("snape");
        expectedAllowedAudiences = ImmutableSet.of("snape");
        assertThat(tokenValidator.audience().validate(HEADER), result(Decision.AUTHORIZED, jwt));

        when(config.audience()).thenReturn("moody");
        expectedAllowedAudiences = ImmutableSet.of("moody", "sprout");
        assertThat(tokenValidator.audience("sprout").validate(HEADER), result(Decision.AUTHORIZED, jwt));
    }
}
