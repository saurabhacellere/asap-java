package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.service.api.ValidationResult;
import com.google.common.base.MoreObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

final class ValidationResultImpl implements ValidationResult {
    private static final Logger logger = LoggerFactory.getLogger(ValidationResultImpl.class);

    private final Decision decision;
    private final Optional<Jwt> token;
    private final Optional<String> untrustedIssuer;

    private ValidationResultImpl(Decision decision, @Nullable Jwt token, @Nullable String untrustedIssuer) {
        this.decision = requireNonNull(decision, "decision");
        this.token = Optional.ofNullable(token);
        this.untrustedIssuer = Optional.ofNullable(untrustedIssuer);

        logger.debug("ASAP authentication result: decision={}; issuer={}",
                decision,
                (token != null) ? token.getClaims().getIssuer() : untrustedIssuer);

    }

    @Override
    public Decision decision() {
        return decision;
    }

    @Override
    public Optional<Jwt> token() {
        return token;
    }

    @Override
    public Optional<String> untrustedIssuer() {
        return untrustedIssuer;
    }

    static ValidationResult notAuthenticated() {
        return new ValidationResultImpl(Decision.NOT_AUTHENTICATED, null, null);
    }

    static ValidationResult notVerified(String untrustedIssuer) {
        return new ValidationResultImpl(Decision.NOT_VERIFIED, null, untrustedIssuer);
    }

    static ValidationResult notAuthorized(String untrustedIssuer) {
        return new ValidationResultImpl(Decision.NOT_AUTHORIZED, null, untrustedIssuer);
    }

    static ValidationResult authorized(Jwt token) {
        requireNonNull(token, "token");
        return new ValidationResultImpl(Decision.AUTHORIZED, token, null);
    }

    static ValidationResult rejected(String untrustedIssuer) {
        return new ValidationResultImpl(Decision.REJECTED, null, untrustedIssuer);
    }

    static ValidationResult abstain() {
        return new ValidationResultImpl(Decision.ABSTAIN, null, null);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("decision", decision)
                .add("token", token.isPresent() ? "present" : "empty")
                .add("untrustedIssuer", untrustedIssuer)
                .toString();
    }
}

