package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.service.api.AuthorizationBuilder;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.collect.ImmutableSet;

import javax.json.JsonObject;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;

public class AuthorizationBuilderImpl implements AuthorizationBuilder {
    private final AsapConfiguration config;
    private final AuthorizationHeaderGenerator authHeaderGenerator;

    private Optional<String> subject = Optional.empty();
    private Optional<Duration> expiration = Optional.empty();
    private Set<String> audience = ImmutableSet.of();
    private Optional<JsonObject> customClaims = Optional.empty();
    private Optional<Optional<Instant>> notBefore = Optional.empty();

    public AuthorizationBuilderImpl(AsapConfiguration config, AuthorizationHeaderGenerator authHeaderGenerator) {
        this.config = requireNonNull(config, "config");
        this.authHeaderGenerator = requireNonNull(authHeaderGenerator, "authHeaderGenerator");
    }

    @Override
    public AuthorizationBuilder subject(Optional<String> subject) {
        this.subject = requireNonNull(subject);
        return this;
    }

    @Override
    public AuthorizationBuilder audience(Iterable<String> audience) {
        this.audience = ImmutableSet.copyOf(audience);
        return this;
    }

    @Override
    public AuthorizationBuilder expiration(Optional<Duration> expiration) {
        this.expiration = requireNonNull(expiration);
        return this;
    }

    @Override
    public AuthorizationBuilder customClaims(Optional<JsonObject> claims) {
        this.customClaims = requireNonNull(claims);
        return this;
    }

    @Override
    public AuthorizationBuilder notBefore(Optional<Instant> notBefore) {
        this.notBefore = Optional.of(requireNonNull(notBefore));
        return this;
    }

    @Override
    public String build() throws InvalidTokenException, CannotRetrieveKeyException {
        final Jwt jwt = buildJwt();
        return authHeaderGenerator.generateAuthorizationHeader(jwt);
    }

    private Jwt buildJwt() throws InvalidTokenException {
        if (audience.isEmpty()) {
            throw new MissingRequiredClaimException(JwtClaims.RegisteredClaim.AUDIENCE);
        }

        final Instant now = Instant.now();
        final JwtBuilder jwtBuilder = JwtBuilder.newJwt()
                .issuer(config.issuer())
                .keyId(config.keyId())
                .subject(subject)
                .issuedAt(now)
                .audience(audience);

        expiration.ifPresent(ttl -> jwtBuilder.expirationTime(now.plus(ttl)));
        customClaims.ifPresent(jwtBuilder::customClaims);
        notBefore.ifPresent(jwtBuilder::notBefore);
        return jwtBuilder.build();
    }
}
