package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;

import javax.ws.rs.core.SecurityContext;

/**
 * Extends {@link SecurityContext} with a property to retrieve the authenticated {@link Jwt} token.
 */
public interface JwtSecurityContext extends SecurityContext {
    /**
     * @return the authenticated {@link Jwt} token
     */
    Jwt getJwt();
}
