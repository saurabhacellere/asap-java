package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.security.PublicKey;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.atlassian.asap.asap.AsapConstants.ASAP_REQUEST_ATTRIBUTE;
import static com.atlassian.asap.asap.AsapConstants.MAX_TRANSIENT_FAILURES_RETRIES;
import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

/**
 * AuthenticationRequestFilter is a {@link ContainerRequestFilter} that authenticates resources with the ASAP protocol
 * if opted-into by using the {@link Asap} annotation on either a resource package, class, or method.
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationRequestFilter implements ContainerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationRequestFilter.class);

    @SuppressWarnings("checkstyle:VisibilityModifier")
    @Context
    ResourceInfo resourceInfo;

    private final RequestAuthenticator authenticator;
    private final FailureHandler failureHandler;
    private final JwtSecurityContextFactory jwtSecurityContextFactory;

    @SuppressWarnings("WeakerAccess")
    public AuthenticationRequestFilter(RequestAuthenticator authenticator, FailureHandler failureHandler) {
        this(authenticator, failureHandler, new JwtSecurityContextFactoryImpl());
    }

    public AuthenticationRequestFilter(RequestAuthenticator authenticator,
                                       FailureHandler failureHandler,
                                       JwtSecurityContextFactory jwtSecurityContextFactory) {
        this.failureHandler = requireNonNull(failureHandler);
        this.authenticator = requireNonNull(authenticator);
        this.jwtSecurityContextFactory = requireNonNull(jwtSecurityContextFactory);
    }

    @Override
    public void filter(ContainerRequestContext context) {
        mayNeedAuthentication()
                .map(asap -> setProperty(context, ASAP_REQUEST_ATTRIBUTE, asap))
                .flatMap(asap -> authenticateToken(context, asap))
                .map(authenticatedToken -> setProperty(context, AUTHENTIC_JWT_REQUEST_ATTRIBUTE, authenticatedToken))
                .ifPresent(authenticatedToken -> setSecurityContext(context, authenticatedToken));
    }

    private Optional<Jwt> authenticateToken(final ContainerRequestContext context, Asap asap) {
        try {
            return maybeGetAuthorizationHeader(context, asap).flatMap(header -> authenticateToken(context, header));
        } catch (PermanentAuthenticationFailedException e) {
            failureHandler.onPermanentAuthenticationFailure(context, e);
            return empty();
        }
    }

    private static Optional<String> maybeGetAuthorizationHeader(final ContainerRequestContext context, Asap asap)
            throws PermanentAuthenticationFailedException {
        final String authorizationHeader = context.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (asap.mandatory() && authorizationHeader == null) {
            throw new PermanentAuthenticationFailedException("Authorization header is missing");
        }
        return Optional.ofNullable(authorizationHeader);
    }

    private Optional<Jwt> authenticateToken(final ContainerRequestContext context, String authorizationHeader) {
        for (int tryCounter = 0; tryCounter < MAX_TRANSIENT_FAILURES_RETRIES; tryCounter++) {
            try {
                Jwt authenticJwt = authenticator.authenticateRequest(authorizationHeader);
                LOG.trace("Accepting authentic token with identifier '{}'", authenticJwt.getClaims().getJwtId());
                return Optional.of(authenticJwt);
            } catch (TransientAuthenticationFailedException e) {
                if (tryCounter >= MAX_TRANSIENT_FAILURES_RETRIES - 1) { // we've done the max number of retries
                    failureHandler.onAuthenticationFailure(context, e);
                    return empty();
                } else {
                    final boolean retry = failureHandler.onTransientAuthenticationFailure(context, e);
                    if (!retry) {
                        return empty(); // request should have already been aborted by the failure handler
                    }
                }
            } catch (PermanentAuthenticationFailedException e) {
                failureHandler.onPermanentAuthenticationFailure(context, e);
                return empty();
            } catch (AuthenticationFailedException e) {
                failureHandler.onAuthenticationFailure(context, e);
                return empty();
            }
        }
        throw new IllegalStateException();
    }

    private void setSecurityContext(ContainerRequestContext context, Jwt authenticatedToken) {
        context.setSecurityContext(
                jwtSecurityContextFactory.createSecurityContext(authenticatedToken, context.getSecurityContext()));
    }

    /**
     * Finds the {@link Asap} token on the resource package, class, or method.
     */
    private Optional<Asap> mayNeedAuthentication() {
        return findFirstNonNullAnnotation(Asap.class,
                () -> resourceInfo.getResourceMethod(),
                () -> resourceInfo.getResourceClass(),
                () -> resourceInfo.getResourceClass().getPackage())
                .filter(Asap::enabled);
    }

    @SafeVarargs
    private static <A extends Annotation> Optional<A> findFirstNonNullAnnotation(Class<A> annotationClass,
                                                                                 Supplier<? extends AnnotatedElement>... annotatedElements) {
        return Stream.of(annotatedElements)
                .map(Supplier::get)
                .filter(e -> e.isAnnotationPresent(annotationClass))
                .map(e -> e.getAnnotation(annotationClass))
                .findFirst();
    }


    /**
     * Use this factory method to create a new AuthorizationRequestFilter instance with the specified audience and
     * public key repository URL.
     *
     * @param audience the non-null audience for this filter
     * @param repoUrl  the ASAP public key repository URL
     * @return the new instance
     */
    public static AuthenticationRequestFilter newInstance(String audience, String repoUrl) {
        return newInstance(new AuthenticationContext(audience, repoUrl));
    }

    /**
     * Use this factory method to create a new AuthorizationRequestFilter instance with the specified audience and
     * public key repository provider.
     *
     * @param audience    the non-null audience for this filter
     * @param keyProvider the ASAP public key provider
     * @return the new instance
     */
    public static AuthenticationRequestFilter newInstance(String audience, KeyProvider<PublicKey> keyProvider) {
        return newInstance(new AuthenticationContext(audience, keyProvider));
    }

    /**
     * Use this factory method to create a new AuthorizationRequestFilter instance with the specified context.
     *
     * @param authenticationContext the non-null authentication context
     * @return the new instance
     */
    public static AuthenticationRequestFilter newInstance(AuthenticationContext authenticationContext) {
        RequestAuthenticatorFactory requestAuthenticatorFactory = new RequestAuthenticatorFactory();
        RequestAuthenticator authenticator = requestAuthenticatorFactory.create(authenticationContext);

        return new AuthenticationRequestFilter(authenticator, new EmptyBodyFailureHandler());
    }

    private static <T> T setProperty(ContainerRequestContext context, String name, T value) {
        context.setProperty(name, value);
        return value;
    }
}
