package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.security.core.Authentication;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ServerWebExchangeAuthenticationConverterTest extends AbstractAsapAuthTest {

    @Test
    public void shouldReturnAuthIfTokenProvided() {
        String token = "abc-123";
        MockServerHttpRequest request = MockServerHttpRequest
                .get("/path")
                .header(HttpHeaders.AUTHORIZATION, JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX + token)
                .build();
        Authentication actual = new ServerWebExchangeAuthenticationConverter().apply(createExchange(request)).block();
        UnverifiedBearerToken expected = new UnverifiedBearerToken(token);

        assertThat(expected, new ReflectionEquals(actual));

    }

    @Test
    public void shouldReturnEmptyIfNoTokenProvided() {
        MockServerHttpRequest request = MockServerHttpRequest
                .get("/path")
                .build();
        Authentication authentication = new ServerWebExchangeAuthenticationConverter().apply(createExchange(request)).block();

        assertNull(authentication);
    }
}