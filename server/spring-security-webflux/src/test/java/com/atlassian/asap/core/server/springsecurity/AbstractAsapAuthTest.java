package com.atlassian.asap.core.server.springsecurity;

import org.mockito.Mockito;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.mock.http.server.reactive.MockServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebHandler;
import org.springframework.web.server.adapter.HttpWebHandlerAdapter;

public abstract class AbstractAsapAuthTest {
    ServerWebExchange createExchange(ServerHttpRequest request) {
        TestHttpWebHandlerAdapter adapter = new TestHttpWebHandlerAdapter(Mockito.mock(WebHandler.class));
        return adapter.createExchange(request, new MockServerHttpResponse());
    }

    static class TestHttpWebHandlerAdapter extends HttpWebHandlerAdapter {

        TestHttpWebHandlerAdapter(WebHandler delegate) {
            super(delegate);
        }

        @Override
        protected ServerWebExchange createExchange(ServerHttpRequest request, ServerHttpResponse response) {
            return super.createExchange(request, response);
        }
    }
}
