package com.atlassian.asap.core.server.springsecurity;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.ReactiveAuthenticationManagerAdapter;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;

/**
 * creates {@code ReactiveAuthenticationManager} applying given list of {@code AuthenticationProvider}.
 * allows to re-use existing {@code AuthenticationProvider} for the reactive APIs.
 *
 * <p>The list of {@link AuthenticationProvider}s will be successively tried until an
 * <code>AuthenticationProvider</code> indicates it is capable of authenticating the
 * type of <code>Authentication</code> object passed. Authentication will then be
 * attempted with that <code>AuthenticationProvider</code>.
 *
 * <p>If more than one <code>AuthenticationProvider</code> supports the passed
 * <code>Authentication</code> object, the first one able to successfully
 * authenticate the <code>Authentication</code> object determines the
 * <code>result</code>, overriding any possible <code>AuthenticationException</code>
 * thrown by earlier supporting <code>AuthenticationProvider</code>s.
 * On successful authentication, no subsequent <code>AuthenticationProvider</code>s
 * will be tried.
 * If authentication was not successful by any supporting
 * <code>AuthenticationProvider</code> the last thrown
 * <code>AuthenticationException</code> will be rethrown.
 *
 * @see org.springframework.security.authentication.AuthenticationProvider
 */
public class AuthenticationProviderReactiveAuthenticationManager extends ReactiveAuthenticationManagerAdapter {
    public AuthenticationProviderReactiveAuthenticationManager(List<AuthenticationProvider> authenticationProviders) {
        super(new ProviderManager(authenticationProviders));
    }

    public AuthenticationProviderReactiveAuthenticationManager(AuthenticationProvider... authenticationProviders) {
        this(Arrays.asList(assertNotNull(authenticationProviders)));
    }

    private static <T> T[] assertNotNull(T... t) {
        Assert.notNull(t);
        Assert.noNullElements(t);
        return t;
    }
}
