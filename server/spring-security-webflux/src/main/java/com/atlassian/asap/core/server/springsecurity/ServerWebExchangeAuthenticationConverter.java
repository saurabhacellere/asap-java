package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;

/**
 *Converts from a {@link ServerWebExchange} to an {@link Authentication} that can be authenticated
 * extracting JWT token from {@code HttpHeaders.AUTHORIZATION} header value.
 */
public class ServerWebExchangeAuthenticationConverter implements Function<ServerWebExchange, Mono<Authentication>> {
    @Override
    public Mono<Authentication> apply(ServerWebExchange exchange) {
        String authorizationHeader = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        String serializedJwt = StringUtils.removeStart(authorizationHeader, JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX);

        return Mono.justOrEmpty(serializedJwt).map(UnverifiedBearerToken::new);
    }
}
