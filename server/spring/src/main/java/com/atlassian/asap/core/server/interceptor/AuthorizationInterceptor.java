package com.atlassian.asap.core.server.interceptor;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.atlassian.asap.asap.AsapConstants.ASAP_REQUEST_ATTRIBUTE;
import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;

/**
 * This class provides authorization of asap annotated Resource methods. It should be used in conjunction with the RequestAuthenticationFilterBean
 * which is needed to provide authentication and to supply the Jwt token
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationInterceptor.class);

    private AsapValidator asapValidator;

    public AuthorizationInterceptor(AsapValidator asapValidator) {
        this.asapValidator = asapValidator;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            Jwt jwt = (Jwt) request.getAttribute(AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
            HandlerMethod hm = (HandlerMethod) handler;
            Optional<Asap> asapAnnotation = findAsapAnnotation(hm.getMethod());

            if (asapAnnotation.isPresent()) {
                Asap asap = asapAnnotation.get();
                request.setAttribute(ASAP_REQUEST_ATTRIBUTE, asap);
                if (!asap.mandatory()) {
                    // no point validating if it isn't mandatory
                    return true;
                }
                if (jwt == null) {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization header is missing.");
                    LOG.debug("Authorization header is missing");
                    return false;
                }
                if (isAuthorized(jwt, asap)) {
                    return true;
                } else {
                    LOG.debug("Request is not authorized.");
                    response.sendError(HttpServletResponse.SC_FORBIDDEN, "Request is not authorized.");
                    return false;
                }
            } else {
                LOG.debug(
                        "method {} of class {} is unsecured, allowing request",
                        hm.getMethod().getName(),
                        hm.getMethod().getDeclaringClass().getSimpleName());
                return true;
            }
        } else {
            // a request for a non-existent url
            return true;
        }
    }

    /**
     * Finds the {@link Asap} token on the resource package, class, or method.
     */
    private Optional<Asap> findAsapAnnotation(Method method) {
        return findFirstNonNullAnnotation(
            Asap.class,
            () -> method,
            () -> method.getDeclaringClass(),
            () -> method.getDeclaringClass().getPackage()
        );
    }

    @SafeVarargs
    private static <A extends Annotation> Optional<A> findFirstNonNullAnnotation(Class<A> annotationClass,
                                                                                 Supplier<? extends AnnotatedElement>... annotatedElements) {
        return Stream.of(annotatedElements)
            .map(Supplier::get)
            .filter(e -> e.isAnnotationPresent(annotationClass))
            .map(e -> e.getAnnotation(annotationClass))
            .findFirst();
    }

    private boolean isAuthorized(final Jwt jwt, final Asap asap) {
        try {
            asapValidator.validate(asap, jwt);
            LOG.trace("Accepting authorized token with identifier '{}'", jwt.getClaims().getJwtId());
            return true;
        } catch (AuthorizationFailedException e) {
            LOG.debug("Authorization failed", e);
            return false;
        }
    }
}
