package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.exception.TokenExpiredException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CachingJwtValidatorTest {

    private static final String ENCODED_JWT = "not-actually-a-jwt";
    private static final String LONG_ENCODED_JWT = "this is a long jwt that has more than 20 characters";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private JwtValidator delegate;

    @Mock
    private JwtRevalidator revalidator;

    private JwtValidator cachingValidator;

    private Jwt jwt;

    @Before
    public void setup() {
        cachingValidator = new CachingJwtValidator(
                delegate,
                revalidator,
                1000,
                30,
                20
        );

        jwt = JwtBuilder.newJwt()
                .keyId("key-id")
                .issuer("mock-issuer")
                .audience("mock-audience")
                .expirationTime(Instant.ofEpochMilli(10000))
                .build();

    }

    @Test
    public void cachingValidator_shouldCacheJwt() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(anyString())).thenReturn(jwt);

        assertThat(cachingValidator.readAndValidate(ENCODED_JWT), equalTo(jwt));
        assertThat(cachingValidator.readAndValidate(ENCODED_JWT), equalTo(jwt));

        verify(delegate).readAndValidate(ENCODED_JWT);
    }

    @Test
    public void cachingValidator_shouldNotCacheLongJwt() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(anyString())).thenReturn(jwt);

        assertThat(cachingValidator.readAndValidate(LONG_ENCODED_JWT), equalTo(jwt));
        assertThat(cachingValidator.readAndValidate(LONG_ENCODED_JWT), equalTo(jwt));

        verify(delegate, times(2)).readAndValidate(LONG_ENCODED_JWT);
    }

    @Test(expected = TokenExpiredException.class)
    public void cachingValidator_shouldCheckExpiry() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(ENCODED_JWT)).thenReturn(jwt);
        assertThat(cachingValidator.readAndValidate(ENCODED_JWT), equalTo(jwt));

        doThrow(new TokenExpiredException(Instant.ofEpochMilli(1000), Instant.ofEpochMilli(2000))).when(revalidator).revalidateClaims(jwt);
        cachingValidator.readAndValidate(ENCODED_JWT);
    }

    @Test(expected = InvalidTokenException.class)
    public void cachingValidator_throwsUnwrappedInvalidTokenException() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(ENCODED_JWT))
                .thenThrow(new TokenExpiredException(Instant.ofEpochMilli(1), Instant.ofEpochMilli(2)));

        cachingValidator.readAndValidate(ENCODED_JWT);
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void cachingValidator_throwsUnwrappedCannotRetrieveKeyException() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(ENCODED_JWT))
                .thenThrow(new CannotRetrieveKeyException("Something went wrong"));

        cachingValidator.readAndValidate(ENCODED_JWT);
    }

    @Test(expected = NullPointerException.class)
    public void cachingValidator_throwsUnwrappedRuntimeException() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.readAndValidate(ENCODED_JWT))
                .thenThrow(new NullPointerException("Null values cause exceptions"));

        cachingValidator.readAndValidate(ENCODED_JWT);
    }
}
