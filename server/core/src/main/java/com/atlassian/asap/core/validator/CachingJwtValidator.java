package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * An implementation of {@link JwtValidator} that wraps an underlying {@link JwtValidatorImpl} in a cache. The cache is
 * of the raw JWT String to the deserialised and validated JWT object. On each cache read, the claims of the JWT will
 * be re-validated in order to ensure that any time-dependent claims are still valid.
 *
 * <p>This class will provide performance improvements for servers that have clients who re-use ASAP tokens between
 * requests.
 */
public class CachingJwtValidator implements JwtValidator {

    private final JwtValidator delegate;
    private final JwtRevalidator revalidator;
    private final Cache<String, Jwt> tokenCache;

    private final int maxEntryLength;

    public CachingJwtValidator(
            final JwtValidator delegate,
            final JwtRevalidator revalidator,
            int maxCacheSize,
            int cacheExpirySec,
            int maxEntryLength
    ) {
        this.delegate = delegate;
        this.revalidator = revalidator;
        this.maxEntryLength = maxEntryLength;
        tokenCache = CacheBuilder.newBuilder()
                .maximumSize(maxCacheSize)
                .expireAfterWrite(cacheExpirySec, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public Jwt readAndValidate(String serializedJwt) throws InvalidTokenException, CannotRetrieveKeyException {
        if (serializedJwt.length() >= maxEntryLength) {
            return delegate.readAndValidate(serializedJwt);
        }

        Optional<Jwt> maybeJwt = Optional.ofNullable(tokenCache.getIfPresent(serializedJwt));

        if (maybeJwt.isPresent()) {
            Jwt jwt = maybeJwt.get();
            revalidator.revalidateClaims(jwt);
            return jwt;
        }

        Jwt jwt = delegate.readAndValidate(serializedJwt);

        tokenCache.put(serializedJwt, jwt);

        return jwt;
    }

    @Override
    public Optional<String> determineUnverifiedIssuer(String serializedJwt) {
        return delegate.determineUnverifiedIssuer(serializedJwt);
    }
}
