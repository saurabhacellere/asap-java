package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import com.google.common.collect.ImmutableSet;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Implements {@link AbstractRequestAuthorizationFilter} by using two whitelists of authorized subjects and
 * issuers.
 */
public class WhitelistRequestAuthorizationFilter extends AbstractRequestAuthorizationFilter {
    private final Set<String> authorizedSubjects;
    private final Set<String> authorizedIssuers;

    /**
     * Constructs an authorization filter that only accepts tokens where the effective subject and the issuer
     * belong to the respective whitelists.
     *
     * @param authorizedSubjects effective subjects must belong to this set to be authorized
     * @param authorizedIssuers  issuers must belong to this set to be authorized
     */
    public WhitelistRequestAuthorizationFilter(Set<String> authorizedSubjects, Set<String> authorizedIssuers) {
        this.authorizedSubjects = ImmutableSet.copyOf(authorizedSubjects);
        this.authorizedIssuers = ImmutableSet.copyOf(authorizedIssuers);
    }

    /**
     * Constructs an authorization filter that only accepts tokens where the issuer and effective subject are in the
     * given set.
     *
     * <p>There is no guarantee that the subject and the issuer are the same, just that they are both in the set. If you
     * want to allow only self-signed JWTs from a known set of issuers, consider using
     * {@link IssuerAndSubjectAwareRequestAuthorizationFilter#issuers(Set)} instead.
     *
     * @param authorizedSubjects issuers and effective subjects must belong to this set to be authorized
     * @deprecated This constructor has been deprecated because the behaviour is misleading. Please use
     * {@link IssuerAndSubjectAwareRequestAuthorizationFilter#issuers(Set)} instead.
     */
    @Deprecated
    public WhitelistRequestAuthorizationFilter(Set<String> authorizedSubjects) {
        this(authorizedSubjects, authorizedSubjects);
    }

    @Override
    protected boolean isAuthorized(HttpServletRequest request, Jwt jwt) {
        // authorize issuer
        boolean issuerIsAuthorized = authorizedIssuers.contains(jwt.getClaims().getIssuer());

        // authorize subject
        String effectiveSubject = jwt.getClaims().getSubject().orElse(jwt.getClaims().getIssuer());
        boolean subjectIsAuthorized = authorizedSubjects.contains(effectiveSubject);

        return issuerIsAuthorized && subjectIsAuthorized;
    }
}
