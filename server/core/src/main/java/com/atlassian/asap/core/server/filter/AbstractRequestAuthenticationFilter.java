package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.JwtConstants;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A servlet filter that accepts or rejects requests based on the authenticity and validity of the JWT access token.
 * It only performs authentication, but it does not authorize the request.
 */
public abstract class AbstractRequestAuthenticationFilter implements Filter {
    /**
     * Upon successful authentication, the authentic {@link Jwt} token is saved in the session under this key,
     * so other filters down the chain can access it, e.g., {@link AbstractRequestAuthorizationFilter}.
     */
    public static final String AUTHENTIC_JWT_REQUEST_ATTRIBUTE = "asap.authentic.jwt";

    private static final Logger logger = LoggerFactory.getLogger(AbstractRequestAuthenticationFilter.class);

    private RequestAuthenticator requestAuthenticator;
    private boolean allowAnonymousRequests;

    public AbstractRequestAuthenticationFilter() {
        this(false);
    }

    /**
     * Use this constructor if you have unsecured endpoints in your application and therefore need to allow unauthenticated requests through the filter chain.
     * This functionality is only viable if you also include a handler interceptor which will enforce Controller level authorization.
     * @param allowAnonymousRequests if true then requests with no authorization header will be allowed through the filter chain
     */
    public AbstractRequestAuthenticationFilter(boolean allowAnonymousRequests) {
        this.allowAnonymousRequests = allowAnonymousRequests;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        requestAuthenticator = getRequestAuthenticator(filterConfig);
    }

    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        Preconditions.checkState(requestAuthenticator != null, "Filter has not been initialized");

        // will fail if the request is not an HTTP request
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String authorizationHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isBlank(authorizationHeader)) {
            if (allowAnonymousRequests) {
                logger.debug("Allowing request with no JWT token.");
                onAuthenticationSuccess(null, httpRequest, httpResponse, chain);
            } else {
                // log at debug level because this is caused by invalid input
                logger.debug("Request rejected because JWT token cannot be found");
                onAuthenticationFailure(httpRequest, httpResponse, chain);
            }
        } else {
            try {
                Jwt authenticJwt = requestAuthenticator.authenticateRequest(authorizationHeader);
                logger.trace("Accepting authentic token with identifier {}", authenticJwt.getClaims().getJwtId());
                saveToken(request, authenticJwt);
                onAuthenticationSuccess(authenticJwt, httpRequest, httpResponse, chain);
            } catch (TransientAuthenticationFailedException e) {
                // log at debug level because this can be caused by invalid input
                logger.debug("Request rejected because JWT token could not be verified at this time", e);
                onAuthenticationError(httpRequest, httpResponse, chain, e);
            } catch (AuthenticationFailedException e) {
                // log at debug level because this can be caused by invalid input
                logger.debug("Request rejected because JWT token cannot be verified", e);
                onAuthenticationFailure(httpRequest, httpResponse, chain);
            }
        }
    }

    /**
     * Save the authentic token in the request to make it available for other filters down the chain. Subclasses
     * can override/decorate this method to save the token somewhere else.
     */
    protected void saveToken(ServletRequest request, Jwt authenticJwt) {
        request.setAttribute(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, authenticJwt);
    }

    /**
     * Handles the failure of request authentication (e.g., invalid token). The default implementation
     * is to return a 401. Override and decorate as necessary.
     */
    protected void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        response.sendError(HttpStatus.SC_UNAUTHORIZED);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, JwtConstants.BEARER_AUTHENTICATION_SCHEME);
    }

    /**
     * Handles an unexpected error during request authentication (such as a failure when retrieving the required public
     * key).  The default implementation is to return a 503. Override and decorate as necessary.
     */
    protected void onAuthenticationError(HttpServletRequest request, HttpServletResponse response, FilterChain chain, TransientAuthenticationFailedException e)
            throws IOException, ServletException {
        // log the error to assist with troubleshooting
        logger.error("An error occurred while authenticating this request", e);
        response.sendError(HttpStatus.SC_SERVICE_UNAVAILABLE);
    }

    /**
     * Handles the acceptance of an authentic request. The default implementation is to pass the request down the
     * filter chain. Override and decorate this as necessary. For instance, some applications may want to
     * store the JWT token or parts of it. Another filter may implement authorization, see for example
     * {@link AbstractRequestAuthorizationFilter}.
     */
    protected void onAuthenticationSuccess(Jwt authenticJwt, HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // nothing to do
    }

    /**
     * Override this to supply your RequestAuthenticator.
     */
    protected abstract RequestAuthenticator getRequestAuthenticator(FilterConfig filterConfig);
}
