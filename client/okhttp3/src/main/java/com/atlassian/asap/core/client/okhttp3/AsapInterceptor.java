package com.atlassian.asap.core.client.okhttp3;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import javax.annotation.Nonnull;
import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * Use this class to add the "Authorization" header to requests for a okhttp3 OkHttpClient, unless there's already an
 * "Authorization" header.
 *
 * @since 2.15
 */
public class AsapInterceptor implements Interceptor {

    private final Jwt jwtPrototype;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    public AsapInterceptor(@Nonnull Jwt jwtPrototype, @Nonnull AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this.jwtPrototype = requireNonNull(jwtPrototype, "jwtPrototype");
        this.authorizationHeaderGenerator = requireNonNull(authorizationHeaderGenerator, "authorizationHeaderGenerator");
    }

    @Override
    public Response intercept(@Nonnull Chain chain) throws IOException {
        Request request = chain.request();
        if (request.header("Authorization") != null) {
            return chain.proceed(request);
        }

        Jwt jwt = JwtBuilder.newFromPrototype(jwtPrototype).build();
        String header = generateAuthorizationHeader(jwt);
        Request requestWithAuth = request.newBuilder()
                .header("Authorization", header)
                .build();
        return chain.proceed(requestWithAuth);
    }

    private String generateAuthorizationHeader(Jwt jwt) {
        try {
            return authorizationHeaderGenerator.generateAuthorizationHeader(jwt);
        } catch (CannotRetrieveKeyException | InvalidTokenException e) {
            throw new AsapInterceptorException("Exception generating ASAP authorization header in interceptor", e);
        }
    }
}
