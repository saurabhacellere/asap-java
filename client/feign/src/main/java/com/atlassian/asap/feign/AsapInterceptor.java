package com.atlassian.asap.feign;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.Objects;

/**
 * Decorates a Feign request with ASAP credentials.
 */
public class AsapInterceptor implements RequestInterceptor {
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;
    private final Jwt jwtPrototype;

    public AsapInterceptor(AuthorizationHeaderGenerator authorizationHeaderGenerator, Jwt jwtPrototype) {
        this.authorizationHeaderGenerator = Objects.requireNonNull(authorizationHeaderGenerator);
        this.jwtPrototype = Objects.requireNonNull(jwtPrototype);
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        Jwt jwt = JwtBuilder.newFromPrototype(jwtPrototype).build();
        try {
            requestTemplate.header("Authorization", authorizationHeaderGenerator.generateAuthorizationHeader(jwt));
        } catch (InvalidTokenException e) {
            throw new RuntimeException("Invalid prototype token", e);
        } catch (CannotRetrieveKeyException e) {
            e.printStackTrace();
        }
    }
}
