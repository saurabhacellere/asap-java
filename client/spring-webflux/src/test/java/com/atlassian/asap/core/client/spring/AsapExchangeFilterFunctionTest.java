package com.atlassian.asap.core.client.spring;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.GET;

public class AsapExchangeFilterFunctionTest {

  private static final String AUTHORIZATION_HEADER_VALUE = "some-value";

  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();

  @Mock
  private AuthorizationHeaderGenerator authorizationHeaderGenerator;

  private Jwt jwtPrototype = JwtBuilder.newJwt()
      .audience("audience").issuer("issuer1").keyId("issuer1/rsa-key-for-tests")
      .build();

  @Test
  public void shouldAddAuthorizationHeader() throws Exception {
    when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

    ClientRequest request = ClientRequest.method(GET, URI.create("http://example.com")).build();

    final AsapExchangeFilterFunction filterFunction = new AsapExchangeFilterFunction(
        authorizationHeaderGenerator, jwtPrototype);

    filterFunction.filter(request, r -> {
      final List<String> authorizationHeaders = r.headers().getOrDefault(HttpHeaders.AUTHORIZATION,
          Collections.emptyList());

      assertThat(authorizationHeaders.get(0), is(AUTHORIZATION_HEADER_VALUE));

      return Mono.just(mock(ClientResponse.class));
    }).block();
  }

  @Test
  public void shouldNotReplaceExistingAuthorizationHeader() throws Exception {
    when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

    final String existingAuthorizationHeader = "existingAuthorizationHeader";
    ClientRequest request = ClientRequest.method(GET, URI.create("http://example.com"))
        .header(HttpHeaders.AUTHORIZATION, existingAuthorizationHeader)
        .build();

    final AsapExchangeFilterFunction filterFunction = new AsapExchangeFilterFunction(
        authorizationHeaderGenerator, jwtPrototype);

    filterFunction.filter(request, r -> {
      final List<String> authorizationHeaders = r.headers().getOrDefault(HttpHeaders.AUTHORIZATION,
          Collections.emptyList());

      assertThat(authorizationHeaders.get(0), is(existingAuthorizationHeader));

      return Mono.just(mock(ClientResponse.class));
    }).block();
  }

  @Test
  public void shouldReturnErrorIfAuthorizationGenerationFails() throws Exception {
    when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenThrow(new RuntimeException());

    final String existingAuthorizationHeader = "existingAuthorizationHeader";
    final ClientRequest request = ClientRequest.method(GET, URI.create("http://example.com"))
        .header(HttpHeaders.AUTHORIZATION, existingAuthorizationHeader)
        .build();
    final ExchangeFunction exchangeFunction = r -> Mono.just(mock(ClientResponse.class));

    final AsapExchangeFilterFunction filterFunction = new AsapExchangeFilterFunction(
        authorizationHeaderGenerator, jwtPrototype);

    StepVerifier.create(filterFunction.filter(request, exchangeFunction))
        .expectError(RuntimeException.class);
  }
}