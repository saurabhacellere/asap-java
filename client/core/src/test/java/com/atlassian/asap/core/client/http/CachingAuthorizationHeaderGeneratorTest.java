package com.atlassian.asap.core.client.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.SigningAlgorithm;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.exception.InvalidClaimException;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.json.Json;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.oneOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CachingAuthorizationHeaderGeneratorTest {

    public static final int TEST_MAX_CACHE_SIZE = 5;
    public static final Duration TEST_EXPIRY = Duration.ofMinutes(1);
    public static final Duration TEST_EXPIRY_BUFFER = Duration.ofSeconds(10);
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private AuthorizationHeaderGenerator delegate;
    @Mock
    private Clock clock;

    private Instant now;

    private CachingAuthorizationHeaderGenerator generator;

    @Before
    public void setUp() throws Exception {
        when(delegate.generateAuthorizationHeader(any())).thenAnswer(args -> UUID.randomUUID().toString());
        when(clock.instant()).thenAnswer(args -> now);
        now = Instant.ofEpochSecond(1000000000L);
        generator = new CachingAuthorizationHeaderGenerator(
                delegate,
                clock,
                TEST_MAX_CACHE_SIZE,
                TEST_EXPIRY,
                TEST_EXPIRY_BUFFER
        );
    }

    @Test
    public void simpleJwtIsReused() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first, notNullValue());
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(second, equalTo(first));
        verify(delegate, times(1)).generateAuthorizationHeader(isA(Jwt.class));
    }

    @Test
    public void cachedSeparatelyByKeyId() throws CannotRetrieveKeyException, InvalidTokenException {
        String first0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key0")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first0, notNullValue());
        String first1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key1")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first1, not(equalTo(first0)));
        String second0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key0")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first0, equalTo(second0));
        String second1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key1")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first1, equalTo(second1));
    }

    @Test
    public void cachedSeparatelyByIssuer() throws CannotRetrieveKeyException, InvalidTokenException {
        String first0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer0")
                .audience("audience")
                .build());
        assertThat(first0, notNullValue());
        String first1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer1")
                .audience("audience")
                .build());
        assertThat(first1, not(equalTo(first0)));
        String second0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer0")
                .audience("audience")
                .build());
        assertThat(first0, equalTo(second0));
        String second1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer1")
                .audience("audience")
                .build());
        assertThat(first1, equalTo(second1));
    }

    @Test
    public void cachedSeparatelyByAudience() throws CannotRetrieveKeyException, InvalidTokenException {
        String first0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience0")
                .build());
        assertThat(first0, notNullValue());
        String first1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience1")
                .build());
        assertThat(first1, not(equalTo(first0)));
        String second0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience0")
                .build());
        assertThat(first0, equalTo(second0));
        String second1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience1")
                .build());
        assertThat(first1, equalTo(second1));
    }

    @Test
    public void cacheUsesMaxSize() throws CannotRetrieveKeyException, InvalidTokenException {
        String[] tokens = new String[TEST_MAX_CACHE_SIZE];
        for (int i = 0; i < TEST_MAX_CACHE_SIZE; i++) {
            tokens[i] = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                    .keyId("issuer/key")
                    .issuer("issuer")
                    .audience("audience" + i)
                    .build());
        }
        assertThat(ImmutableSet.copyOf(tokens), hasSize(TEST_MAX_CACHE_SIZE));
        String aud0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience" + 0)
                .build());
        assertThat(aud0, equalTo(tokens[0]));
        String audN1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience" + (TEST_MAX_CACHE_SIZE + 1))
                .build());
        assertThat(audN1, not(oneOf(tokens)));
        aud0 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience" + 0)
                .build());
        assertThat(aud0, equalTo(tokens[0]));
        String aud1 = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience" + 1)
                .build());
        assertThat(aud1, not(equalTo(tokens[1])));

    }

    @Test
    public void customClaimsJwtIsNotReused() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .customClaims(Json.createObjectBuilder()
                        .add("custom", "yo")
                        .build())
                .build());
        assertThat(first, notNullValue());
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .customClaims(Json.createObjectBuilder()
                        .add("custom", "yo")
                        .build())
                .build());
        assertThat(second, not(equalTo(first)));
        verify(delegate, times(2)).generateAuthorizationHeader(isA(Jwt.class));
    }

    @Test
    public void customAlgorithmJwtIsNotReused() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .algorithm(SigningAlgorithm.ES384)
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first, notNullValue());
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .algorithm(SigningAlgorithm.ES384)
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(second, not(equalTo(first)));
        verify(delegate, times(2)).generateAuthorizationHeader(isA(Jwt.class));
    }

    @Test
    public void longerThanDefaultExpiryJwtIsNotReused() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .issuedAt(now)
                .notBefore(Optional.of(now))
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME).plus(1, ChronoUnit.SECONDS))
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first, notNullValue());
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .issuedAt(now)
                .notBefore(Optional.of(now))
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME).plus(1, ChronoUnit.SECONDS))
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(second, not(equalTo(first)));
        verify(delegate, times(2)).generateAuthorizationHeader(isA(Jwt.class));
    }

    @Test
    public void shorterThanDefaultExpiryJwtIsNotReused() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .issuedAt(now)
                .notBefore(Optional.of(now))
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME).minus(1, ChronoUnit.SECONDS))
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first, notNullValue());
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .issuedAt(now)
                .notBefore(Optional.of(now))
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME).minus(1, ChronoUnit.SECONDS))
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(second, not(equalTo(first)));
        verify(delegate, times(2)).generateAuthorizationHeader(isA(Jwt.class));
    }

    @Test
    public void jwtIsNotReusedWithinExpiryBuffer() throws CannotRetrieveKeyException, InvalidTokenException {
        String first = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(first, notNullValue());

        now = now.plus(TEST_EXPIRY).minus(TEST_EXPIRY_BUFFER);
        String second = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(second, equalTo(first));

        now = now.plus(1, ChronoUnit.MILLIS);
        String third = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(third, not(equalTo(first)));

        now = now.plus(TEST_EXPIRY).minus(TEST_EXPIRY_BUFFER);
        String fourth = generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
        assertThat(fourth, equalTo(third));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void delegateCannotRetrieveKeyIsPropagated() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.generateAuthorizationHeader(any())).thenThrow(new CannotRetrieveKeyException(""));
        generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
    }

    @Test(expected = InvalidTokenException.class)
    public void delegateInvalidTokenIsPropagated() throws CannotRetrieveKeyException, InvalidTokenException {
        when(delegate.generateAuthorizationHeader(any())).thenThrow(new InvalidClaimException(JwtClaims.RegisteredClaim.AUDIENCE, ""));
        generator.generateAuthorizationHeader(JwtBuilder.newJwt()
                .keyId("issuer/key")
                .issuer("issuer")
                .audience("audience")
                .build());
    }

}
