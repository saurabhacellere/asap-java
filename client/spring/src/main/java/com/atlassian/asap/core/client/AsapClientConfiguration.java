package com.atlassian.asap.core.client;

import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.privatekey.DataUriKeyProvider;
import com.atlassian.asap.core.serializer.JwtSerializer;
import com.atlassian.asap.nimbus.serializer.NimbusJwtSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.PrivateKey;

/**
 * Client-side ASAP configuration.
 */
@Configuration
public class AsapClientConfiguration {
    private final String issuer;

    private final String keyId;

    @Autowired
    AsapClientConfiguration(@Value("${asap.issuer}") String issuer,
                            @Value("${asap.key_id}") String keyId) {
        this.issuer = issuer;
        this.keyId = keyId;
    }

    /**
     * Definition of the provider of private keys.
     *
     * @param privateKeyDataUri a private key, in the data URI format
     * @return a provider of private keys that provides the key passed as argument
     */
    @Bean
    public KeyProvider<PrivateKey> privateKeyProvider(@Value("${asap.private_key}") String privateKeyDataUri) {
        final URI parsedPrivateKeyDataUri;
        try {
            parsedPrivateKeyDataUri = new URI(privateKeyDataUri);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Cannot parse private data URI argument as a URI");
        }
        return new DataUriKeyProvider(parsedPrivateKeyDataUri, new DataUriKeyReader());
    }

    /**
     * Definition of the {@link JwtSerializer} bean.
     *
     * @return an instance of {@link JwtSerializer}
     */
    @Bean
    public JwtSerializer jwtSerializer() {
        return new NimbusJwtSerializer();
    }

    /**
     * Definition of the {@link AuthorizationHeaderGenerator} bean.
     *
     * @param jwtSerializer      the token serializer
     * @param privateKeyProvider the provider of the private key used to sign the token
     * @return an instance of {@link AuthorizationHeaderGenerator}
     */
    @Bean
    public AuthorizationHeaderGenerator authorizationHeaderGenerator(JwtSerializer jwtSerializer,
                                                                     KeyProvider<PrivateKey> privateKeyProvider) {
        return new AuthorizationHeaderGeneratorImpl(jwtSerializer, privateKeyProvider);
    }

    /**
     * @return the issuer
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * @return the key identifier
     */
    public String getKeyId() {
        return keyId;
    }
}
