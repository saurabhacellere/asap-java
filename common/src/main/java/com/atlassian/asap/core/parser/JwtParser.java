package com.atlassian.asap.core.parser;

import com.atlassian.asap.core.exception.JwtParseException;
import com.atlassian.asap.core.exception.UnsupportedAlgorithmException;

import java.util.Optional;

/**
 * Parses a JWT token without verifying its signature or checking the validity of its claims.
 */
public interface JwtParser {
    /**
     * Parses the encoded JWT message from {@link String}, and returns a verifiable JWT object without
     * verifying its signature or validating its claims. All the required headers and claims must be present.
     *
     * @param serializedJwt a JSON Web Token
     * @return a {@link VerifiableJwt} that has all the required claims and headers
     * @throws JwtParseException             if the JWT string was malformed (see subclasses)
     * @throws UnsupportedAlgorithmException if the signature algorithm is not recognised
     */
    VerifiableJwt parse(String serializedJwt) throws JwtParseException, UnsupportedAlgorithmException;

    /**
     * Extracts the issuer, if at all possible, from the claims section of the given serialized JWT.
     * This will NOT validate anything in the JWT, and is only intended to be used to assist in returning useful
     * authentication failure messages to clients, not for real issuer validation.
     *
     * @param serializedJwt a JSON Web Token
     * @return the issuer claim from that JWT, or else empty if none could be parsed or found
     */
    Optional<String> determineUnverifiedIssuer(String serializedJwt);
}
