package com.atlassian.asap.core.serializer;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.core.exception.SigningException;
import com.atlassian.asap.core.exception.UnsupportedAlgorithmException;

import java.security.PrivateKey;

/**
 * Signs and serialises JWT tokens.
 */
public interface JwtSerializer {
    /**
     * Write the JWT with an appropriate signature.
     *
     * @param jwt        the JWT object to serialize as signed JWT
     * @param privateKey the private key to use to sign the JWT
     * @return a String serialisation of the JWT token
     * @throws SigningException              if there was a problem signing the JWT
     * @throws UnsupportedAlgorithmException if the algorithm in the JWT headers is not supported
     */
    String serialize(Jwt jwt, PrivateKey privateKey) throws SigningException, UnsupportedAlgorithmException;
}
