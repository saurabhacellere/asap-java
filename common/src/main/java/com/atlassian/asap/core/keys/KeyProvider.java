package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.security.Key;

public interface KeyProvider<K extends Key> {
    /**
     * Provides a Key for a validated key identifier.
     *
     * @param keyId the validated keyId of interest which is safe from directory traversal
     * @return the relevant key if found
     * @throws CannotRetrieveKeyException if the key couldn't be retrieved or found.
     */
    K getKey(ValidatedKeyId keyId) throws CannotRetrieveKeyException;
}
