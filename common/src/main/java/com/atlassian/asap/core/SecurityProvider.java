package com.atlassian.asap.core;


import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Provider;

public final class SecurityProvider {

    private static final Provider PROVIDER = new BouncyCastleProvider();

    /**
     * Returns an instance of {@link java.security.Provider}.
     * All calls to this method will return same instance of provider
     *
     * @return instance of {@link java.security.Provider}.
     */
    public static Provider getProvider() {
        return PROVIDER;
    }

}
