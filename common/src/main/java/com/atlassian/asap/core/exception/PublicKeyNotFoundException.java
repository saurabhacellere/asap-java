package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.net.URI;

/**
 * Thrown when the public key is reported as absent from the key repository.
 *
 * @see PublicKeyRetrievalException
 */
public class PublicKeyNotFoundException extends CannotRetrieveKeyException {
    public PublicKeyNotFoundException(String reason, ValidatedKeyId keyId) {
        super(reason, keyId);
    }

    public PublicKeyNotFoundException(String reason, ValidatedKeyId keyId, URI keyUri) {
        super(reason, keyId, keyUri);
    }
}
