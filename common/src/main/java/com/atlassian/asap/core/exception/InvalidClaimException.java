package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * Thrown if a JWT claim does not match its expected format.
 */
public class InvalidClaimException extends InvalidTokenException {
    private final JwtClaims.RegisteredClaim claim;

    public InvalidClaimException(JwtClaims.RegisteredClaim claim, String message) {
        super(message);
        this.claim = claim;
    }

    public JwtClaims.RegisteredClaim getClaim() {
        return claim;
    }

    @Override
    public String getSafeDetails() {
        return super.getSafeDetails() + " - " + claim;
    }
}
