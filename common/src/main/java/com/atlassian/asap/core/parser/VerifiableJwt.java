package com.atlassian.asap.core.parser;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.InvalidTokenException;

import java.security.PublicKey;

/**
 * A signed JWT that can be verified using its signature and a public key.
 */
public interface VerifiableJwt extends Jwt {
    /**
     * Verify the signature of the given Signed Jwt.
     *
     * @param publicKey the public key to use to verify the signature
     * @throws InvalidTokenException if the signature is invalid or cannot be verified
     */
    void verifySignature(PublicKey publicKey) throws InvalidTokenException;
}
