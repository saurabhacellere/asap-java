package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.annotations.VisibleForTesting;

import java.security.PublicKey;
import java.util.List;

/**
 * <p>Looks up keys sequentially in a given list of key providers.</p>
 *
 * <p>It will throw {@link PublicKeyNotFoundException} if all the providers are
 * available and none of them contains the key. It will throw
 * {@link CannotRetrieveKeyException} if at least one of the providers is down
 * and the key cannot be found in any of the others.</p>
 *
 * <p>Note that the current implementation doesn't perform any caching, in
 * particular it doesn't remember where the key was found last time. That may
 * reduce the effectiveness of the provider's http cache if it comes later
 * in the keyProviderChain.</p>
 * @deprecated Since 2.19.5, user {@link com.atlassian.asap.core.keys.ChainedKeyProvider} instead
 */
@Deprecated
public class ChainedKeyProvider implements KeyProvider<PublicKey> {

    private final com.atlassian.asap.core.keys.ChainedKeyProvider<PublicKey> delegate;

    @VisibleForTesting
    ChainedKeyProvider(List<KeyProvider<PublicKey>> keyProviderChain) {
        this.delegate = new com.atlassian.asap.core.keys.ChainedKeyProvider<>(
                keyProviderChain,
                keyId -> new PublicKeyNotFoundException("None of the chained key providers contains the key", keyId));
    }

    /**
     * Create a chained key provider representing a given list of key repository providers.
     * If there is only one provider in the chain, then return the provider instead of wrapping it.
     *
     * @param keyProviderChain set of chained key repository providers in order
     * @return the chained key provider if there are more than one providers, or a key provider when it is
     * the only provider in the chain
     */
    public static KeyProvider<PublicKey> createChainedKeyProvider(List<KeyProvider<PublicKey>> keyProviderChain) {
        if (keyProviderChain.size() == 1) {
            return keyProviderChain.get(0);
        }
        return new ChainedKeyProvider(keyProviderChain);
    }

    @Override
    public PublicKey getKey(ValidatedKeyId keyId) throws CannotRetrieveKeyException {
        return delegate.getKey(keyId);
    }

    /**
     * @return the chain of key providers in the same order as they are consulted to resolve a key
     */
    public List<KeyProvider<PublicKey>> getKeyProviderChain() {
        return delegate.getKeyProviderChain();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
