package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.KeyReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.Objects;

import static com.atlassian.asap.core.keys.ClassPathUri.classPathUri;

/**
 * Retrieves public keys from the classpath. The use of this provider in production code is discouraged because it
 * does not make it possible to quickly add, remove or rotate keys. However, it may be useful for testing purposes.
 */
public class ClasspathPublicKeyProvider implements KeyProvider<PublicKey> {
    private static final Logger logger = LoggerFactory.getLogger(ClasspathPublicKeyProvider.class);

    private final KeyReader keyReader;
    private final String classpathBase;

    /**
     * Create a provider that reads public keys under the given classpath base.
     *
     * @param classpathBase classpath prefix, must end on a slash
     * @param keyReader     key reader
     */
    public ClasspathPublicKeyProvider(String classpathBase, KeyReader keyReader) {
        this.keyReader = Objects.requireNonNull(keyReader);
        this.classpathBase = Objects.requireNonNull(classpathBase);
        Preconditions.checkArgument(classpathBase.endsWith("/"), "Classpath prefix must end with a slash");
    }

    @Override
    public PublicKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        String pathToKey = classpathBase + validatedKeyId.getKeyId();

        logger.debug("Reading public key from classpath: {}", pathToKey);

        try (InputStream inputStream = ClasspathPublicKeyProvider.class.getResourceAsStream(pathToKey)) {
            if (inputStream == null) {
                throw new PublicKeyNotFoundException(
                        "Public key not found in the classpath", validatedKeyId, classPathUri(pathToKey));
            }
            try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.US_ASCII)) {
                return keyReader.readPublicKey(reader);
            }
        } catch (IOException e) {
            throw new PublicKeyRetrievalException(
                    "Error retrieving public key from classpath", e, validatedKeyId, classPathUri(pathToKey));
        }
    }

    @VisibleForTesting
    String getClasspathBase() {
        return classpathBase;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
