package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

import java.time.Instant;

/**
 * Thrown when a token was received before the not-before date/time listed in the nbf claim.
 */
public class TokenTooEarlyException extends InvalidTokenException {
    public TokenTooEarlyException(Instant notBefore, Instant now) {
        super(String.format("Not-before time is %s and time is now %s", notBefore, now));
    }
}
