package com.atlassian.asap.asap;

public final class AsapConstants {
    public static final String ASAP_REQUEST_ATTRIBUTE = "asap.annotation";
    public static final int MAX_TRANSIENT_FAILURES_RETRIES = 10;

    private AsapConstants() { }
}
