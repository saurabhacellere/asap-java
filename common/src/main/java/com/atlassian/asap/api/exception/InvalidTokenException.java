package com.atlassian.asap.api.exception;

/**
 * Indicates a JWT was syntactically well formed (parses successfully), but failed to validate.
 */
public abstract class InvalidTokenException extends Exception {
    protected InvalidTokenException(String message) {
        super(message);
    }

    protected InvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }

    protected InvalidTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Returns safe-to-propagate details about this exception, eg. the class name (eg. TokenTooEarlyException,
     * MissingRequiredClaimException, etc.), that we can safely include in any information sent back to the caller
     * (eg. exception messages).  May be extended by descendants to provide additional safe-to-propagate details,
     * where appropriate for more specialised exceptions.
     *
     * @return a string which contains basic details about this exception, and is safe to propagate and log
     */
    public String getSafeDetails() {
        return getClass().getSimpleName();
    }
}
