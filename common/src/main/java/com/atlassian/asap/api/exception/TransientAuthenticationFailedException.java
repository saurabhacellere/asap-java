package com.atlassian.asap.api.exception;

import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.net.URI;

/**
 * Thrown when an incoming HTTP request cannot be authenticated using ASAP, and a retry of a request may succeed.
 * This covers cases such as a temporary inability to retrieve the public key from the key server (like a communication
 * failure or 500 error response).
 */
public class TransientAuthenticationFailedException extends AuthenticationFailedException {
    public TransientAuthenticationFailedException(String message, ValidatedKeyId keyId, URI keyUri, String unverifiedIssuer) {
        super(message, keyId, keyUri, unverifiedIssuer);
    }
}
