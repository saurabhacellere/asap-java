package com.atlassian.asap.api;

import static com.atlassian.asap.api.AlgorithmType.ECDSA;
import static com.atlassian.asap.api.AlgorithmType.RSA;
import static com.atlassian.asap.api.AlgorithmType.RSASSA_PSS;

/**
 * An enumeration of asymmetric JWS algorithms. Values must match the names used in the JWT 'alg' header. Valid values
 * are specified by <a href="https://tools.ietf.org/html/rfc7518">JSON Web Algorithms</a>.
 */
public enum SigningAlgorithm {
    RS256(RSA, 256), RS384(RSA, 384), RS512(RSA, 512),
    ES256(ECDSA, 256), ES384(ECDSA, 384), ES512(ECDSA, 512),
    PS256(RSASSA_PSS, 256), PS384(RSASSA_PSS, 384), PS512(RSASSA_PSS, 512);
    // Do NOT add here symmetric JWS algorithms (like HS256) because the security of the ASAP protocol depends on the
    // use of asymmetric keys, which allow the key used for signing the token to remain a secret.

    private final AlgorithmType type;
    private final int hashSize;

    SigningAlgorithm(AlgorithmType type, int hashSize) {
        this.type = type;
        this.hashSize = hashSize;
    }

    public AlgorithmType type() {
        return type;
    }

    public int hashSize() {
        return hashSize;
    }
}
