package com.atlassian.asap.api.server.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;

/**
 * HTTP servers can use this service to authenticate incoming HTTP requests that include a JWT bearer token in the
 * Authorization header, conforming to the specification of ASAP protocol.
 *
 * @see <a href="http://s2sauth.bitbucket.org/">ASAP Authentication</a>
 */
public interface RequestAuthenticator {
    /**
     * Authenticates a request by validating the given authorizationHeader according to the conventions.
     * defined in ASAP Specification
     *
     * @param authorizationHeader the value of the 'Authorization' header of the request to validate. This 'Authorization' header
     *                            should contain a JWT with the necessary authentication information
     * @return a valid JWT object corresponding to the claims body of the validated token
     * @throws AuthenticationFailedException if there is a problem validating the token in the header.  May be an
     *                                       instance of PermanentAuthenticationFailureException (eg. for an incorrect
     *                                       signature, or if the claims fail to pass the mandatory validations), or an
     *                                       instance of TransientAuthenticationFailedException (when if there is a
     *                                       temporary problem validating the token in the header, eg. a failure to
     *                                       retrieve the required public key)
     */
    Jwt authenticateRequest(String authorizationHeader) throws AuthenticationFailedException;
}
