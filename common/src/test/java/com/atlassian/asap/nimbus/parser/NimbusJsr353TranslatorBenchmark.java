package com.atlassian.asap.nimbus.parser;

import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

public class NimbusJsr353TranslatorBenchmark {
    @Benchmark
    public void nimbusToJsr353(BenchmarkState state) throws ParseException {
        NimbusJsr353Translator.nimbusToJsr353(state.originalNimbus);
    }

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private Object originalNimbus;

        public BenchmarkState() {
            try {
                originalNimbus = new JSONParser(JSONParser.MODE_STRICTEST).parse("{\"key\":[42,3.14,\"foobar\",true,false,null,{}]}");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
