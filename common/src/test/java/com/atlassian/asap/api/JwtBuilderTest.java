package com.atlassian.asap.api;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.Serializable;
import java.time.Instant;
import java.util.Optional;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class JwtBuilderTest {
    public static final Jwt SOME_JWT = JwtBuilder.newJwt()
            .keyId("keyId").issuer("issuer").audience("audience")
            .issuedAt(Instant.EPOCH)
            .build();

    @Test
    public void shouldCreateIdenticalCopyOfPrototype() {
        Jwt jwtCopy = JwtBuilder.copyJwt(SOME_JWT).build();
        assertThat(jwtCopy, equalTo(SOME_JWT));
    }

    @Test
    public void shouldCreateFreshTokenFromPrototype() {
        Jwt freshJwt = JwtBuilder.newFromPrototype(SOME_JWT).build();

        // some claims change
        assertThat(freshJwt.getClaims().getJwtId(), not(equalTo(SOME_JWT.getClaims().getJwtId())));
        assertThat(freshJwt.getClaims().getIssuedAt(), not(equalTo(SOME_JWT.getClaims().getIssuedAt())));

        // other claims/headers don't change
        assertThat(freshJwt.getHeader().getKeyId(), is("keyId"));
        assertThat(freshJwt.getClaims().getIssuer(), is("issuer"));
        assertThat(freshJwt.getClaims().getAudience(), contains("audience"));
    }

    @Test
    public void shouldReturnSerializableJwt() {
        assertThat(SOME_JWT, instanceOf(Serializable.class));

        byte[] serializedJwt = SerializationUtils.serialize((Serializable) SOME_JWT);
        Object deserializedObject = SerializationUtils.deserialize(serializedJwt);
        assertThat(deserializedObject, equalTo(SOME_JWT));
    }

    @Test
    public void customClaimsCanBeIncluded() {
        JsonObject customClaims = Json.createObjectBuilder()
                .add("wizard", "harry")
                .add("number", 42)
                .build();
        Jwt jwt = JwtBuilder.copyJwt(SOME_JWT)
                .customClaims(customClaims)
                .build();
        JsonObject json = jwt.getClaims().getJson();
        assertThat(json.getString("wizard"), is("harry"));
        assertThat(json.getInt("number"), is(42));
    }

    @Test
    public void registeredClaimsTakePrecedenceOverCustomClaims() {
        JsonObject customClaims = Json.createObjectBuilder()
                .add("iss", "should-be-ignored")
                .add("sub", "should-be-ignored")
                .add("nbf", "should-be-ignored")
                .build();
        Jwt jwt = JwtBuilder.copyJwt(SOME_JWT)
                .issuer("issuer1")
                .subject(Optional.empty())
                .notBefore(Optional.empty())
                .customClaims(customClaims).build();

        assertThat(jwt.getClaims().getIssuer(), is("issuer1"));
        assertThat(jwt.getClaims().getSubject(), is(Optional.empty()));
        assertThat(jwt.getClaims().getNotBefore(), is(Optional.empty()));
    }
}
