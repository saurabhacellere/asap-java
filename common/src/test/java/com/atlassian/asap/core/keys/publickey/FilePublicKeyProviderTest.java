package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Reader;
import java.security.PublicKey;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FilePublicKeyProviderTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private PemReader pemReader;
    @Mock
    private PublicKey publicKey;

    private FilePublicKeyProvider provider;

    @Test
    public void shouldReadKeyFromKeyFile() throws Exception {
        temporaryFolder.newFile("some-key-id");

        when(pemReader.readPublicKey(any(Reader.class))).thenReturn(publicKey);

        provider = new FilePublicKeyProvider(temporaryFolder.getRoot(), pemReader);
        PublicKey actualKey = provider.getKey(ValidatedKeyId.validate("some-key-id"));

        assertSame(publicKey, actualKey);

        verify(pemReader).readPublicKey(any(Reader.class));
    }

    @Test(expected = PublicKeyNotFoundException.class)
    public void shouldFailIfKeyFileDoesNotExist() throws Exception {
        provider = new FilePublicKeyProvider(temporaryFolder.getRoot(), pemReader);
        provider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = PublicKeyNotFoundException.class)
    public void shouldFailIfKeyFileIsActuallyADirectory() throws Exception {
        temporaryFolder.newFolder("some-key-id");

        provider = new FilePublicKeyProvider(temporaryFolder.getRoot(), pemReader);
        provider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = PublicKeyRetrievalException.class)
    public void shouldFailIfParsingTheKeyFails() throws Exception {
        temporaryFolder.newFile("some-key-id");

        when(pemReader.readPublicKey(any(Reader.class)))
                .thenThrow(new CannotRetrieveKeyException("cannot parse!"));

        provider = new FilePublicKeyProvider(temporaryFolder.getRoot(), pemReader);
        provider.getKey(ValidatedKeyId.validate("some-key-id"));
    }
}
