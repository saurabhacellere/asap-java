package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.verify.VerificationTimes;

import java.net.URI;

import static org.junit.Assert.fail;

public class HttpPublicKeyProviderIntTest {
    private static final String KEY_ID = "keyId";

    @Rule
    public MockServerRule mockServerRule = new MockServerRule(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private MockServerClient mockServerClient;

    private HttpPublicKeyProvider httpPublicKeyProvider;

    @Before
    public void createProvider() {
        httpPublicKeyProvider = new HttpPublicKeyProvider(
                URI.create("http://localhost:" + mockServerRule.getPort() + "/"),
                HttpPublicKeyProvider.defaultHttpClient(),
                new PemReader(),
                true);
    }

    @Test
    public void shouldRetryOnServerFailure() throws Exception {
        HttpRequest expectedRequest = HttpRequest.request().withPath("/" + KEY_ID);
        mockServerClient
                .when(expectedRequest)
                .respond(HttpResponse.response().withStatusCode(500));

        try {
            httpPublicKeyProvider.getKey(ValidatedKeyId.validate(KEY_ID));
            fail("Should have failed");
        } catch (PublicKeyRetrievalException e) {
            // verify request is retried
            mockServerClient.verify(expectedRequest, VerificationTimes.exactly(3)); // initial + 2 retries
        }
    }
}
