package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.keys.privatekey.EnvironmentVariableKeyProvider.Environment;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ProvideSystemProperty;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.security.PrivateKey;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PrivateKeyProviderFactoryTest {
    private static final String PROPERTY_NAME = "base.name";
    private static final String VALID_KEY_DATA = "data:application/pkcs8;kid=apikey;base64,MDoCAQAwDQYJKoZIhvcNAQEBBQAEJjAkAgEAAgMBGE4CAwEAAQICTGsCAwCMJwIBAgICTGsCAQACAkYU";

    @Rule
    public ProvideSystemProperty provideSystemProperty = new ProvideSystemProperty(PROPERTY_NAME, VALID_KEY_DATA);

    @Mock
    private PemReader pemReader;
    @Mock
    private Environment environment;

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectUnknownSchemes() {
        PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("unknown://baseurl"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectInsecureHttp() {
        PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("http://example.test/"));
    }

    @Test
    public void shouldCreateClasspathProvider() throws Exception {
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("classpath:///asap_private_keys/"));
        assertThat(provider, instanceOf(ClasspathPrivateKeyProvider.class));
        PrivateKey testkey = provider.getKey(ValidatedKeyId.validate("testkey/key.pem"));
        assertThat(testkey.getFormat(), equalTo("PKCS#8"));
    }

    @Test
    public void shouldCreateFileProvider() {
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("file:///some/location/"));
        assertThat(provider, instanceOf(FilePrivateKeyProvider.class));
    }

    @Test
    public void shouldCreateSystemPropertyProvider() throws Exception {
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("sysprop:///" + PROPERTY_NAME));
        assertThat(provider, instanceOf(SystemPropertyKeyProvider.class));
        PrivateKey apikey = provider.getKey(ValidatedKeyId.validate("apikey"));
        assertThat(apikey.getFormat(), equalTo("PKCS#8"));
    }

    @Test
    public void shouldCreateEnvironmentVariableProvider() throws Exception {
        when(environment.getVariable(PROPERTY_NAME)).thenReturn(Optional.of(VALID_KEY_DATA));
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("env:///" + PROPERTY_NAME), environment);
        assertThat(provider, instanceOf(EnvironmentVariableKeyProvider.class));
        PrivateKey apikey = provider.getKey(ValidatedKeyId.validate("apikey"));
        assertThat(apikey.getFormat(), equalTo("PKCS#8"));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldCreateNullProvider() throws Exception {
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(URI.create("null:/"));
        assertThat(provider, instanceOf(NullKeyProvider.class));
        provider.getKey(ValidatedKeyId.validate("apikey"));
    }

    @Test
    public void shouldCreateDataProvider() throws Exception {
        KeyProvider<PrivateKey> provider = PrivateKeyProviderFactory.createPrivateKeyProvider(new URI(VALID_KEY_DATA));
        assertThat(provider, instanceOf(DataUriKeyProvider.class));
        PrivateKey apikey = provider.getKey(ValidatedKeyId.validate("apikey"));
        assertThat(apikey.getFormat(), equalTo("PKCS#8"));
    }
}
