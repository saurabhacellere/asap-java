package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FilePrivateKeyProviderTest {
    public static final String VALID_KID = "valid-kid";

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private PemReader pemReader;
    @Mock
    private RSAPrivateKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromFile() throws Exception {
        temporaryFolder.newFile(VALID_KID);

        KeyProvider<PrivateKey> keyRetriever =
                new FilePrivateKeyProvider(temporaryFolder.getRoot(), pemReader);

        when(pemReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);
        PrivateKey result = keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID));

        assertEquals(privateKey, result);

    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldGetErrorWhenReadingKeyFromNonExistentFile() throws Exception {
        KeyProvider<PrivateKey> keyRetriever =
                new FilePrivateKeyProvider(temporaryFolder.getRoot(), pemReader);

        keyRetriever.getKey(ValidatedKeyId.validate("non-existent"));
    }
}
