package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.InvalidHeaderException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.security.PublicKey;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MirroredKeyProviderTest {
    public static final String KEY_ID = "key-1";
    public static final URI KEY_URI = URI.create("test:" + KEY_ID);

    @Mock
    private KeyProvider<PublicKey> mirror1;
    @Mock
    private KeyProvider<PublicKey> mirror2;
    @Mock
    private PublicKey key1;
    private ValidatedKeyId validatedKeyId;

    private MirroredKeyProvider keyProvider;

    @Before
    public void initialise() throws InvalidHeaderException {
        keyProvider = new MirroredKeyProvider(ImmutableList.of(mirror1, mirror2));
        validatedKeyId = ValidatedKeyId.validate(KEY_ID);
    }

    @Test
    public void shouldSuccessfullyGetKeyFromFirstMirror() throws Exception {
        when(mirror1.getKey(validatedKeyId)).thenReturn(key1);
        assertThat(keyProvider.getKey(validatedKeyId), is(key1));
        verifyZeroInteractions(mirror2);
    }

    @Test
    public void shouldSuccessfullyGetKeyFromSecondMirror() throws Exception {
        when(mirror1.getKey(validatedKeyId)).thenThrow(new PublicKeyRetrievalException("", validatedKeyId, KEY_URI));
        when(mirror2.getKey(validatedKeyId)).thenReturn(key1);
        assertThat(keyProvider.getKey(validatedKeyId), is(key1));
        verify(mirror1).getKey(validatedKeyId);
        verify(mirror2).getKey(validatedKeyId);
    }

    @Test
    public void shouldGiveupIfKeyDoesNotExistInFirstMirror() throws Exception {
        when(mirror1.getKey(validatedKeyId)).thenThrow(new PublicKeyNotFoundException("", validatedKeyId, KEY_URI));
        try {
            keyProvider.getKey(validatedKeyId);
            fail("Should have thrown");
        } catch (CannotRetrieveKeyException e) {
            verify(mirror1).getKey(validatedKeyId);
            verifyZeroInteractions(mirror2);
        }
    }

    @Test
    public void shouldThrowExceptionIfAllMirrorsAreDown() throws Exception {
        when(mirror1.getKey(validatedKeyId)).thenThrow(new CannotRetrieveKeyException(""));
        when(mirror2.getKey(validatedKeyId)).thenThrow(new CannotRetrieveKeyException(""));
        try {
            keyProvider.getKey(validatedKeyId);
            fail("Should have thrown");
        } catch (CannotRetrieveKeyException e) {
            verify(mirror1).getKey(validatedKeyId);
            verify(mirror2).getKey(validatedKeyId);
        }
    }

    @Test
    public void shouldNotWrapForSingleMirror() {
        List<KeyProvider<PublicKey>> providers = ImmutableList.of(mirror1);
        KeyProvider<PublicKey> mirroredProvider = MirroredKeyProvider.createMirroredKeyProvider(providers);
        assertThat(mirroredProvider, sameInstance(mirror1));
    }
}
