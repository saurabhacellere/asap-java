package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Reader;
import java.security.PrivateKey;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StringPrivateKeyProviderTest {

    private static final String KEY_VALUE = "key-value";
    private static final String KEY_ID = "key-id";

    @Mock
    private KeyReader keyReader;

    @Mock
    private PrivateKey key;

    @Test
    public void shouldReadKey() throws Exception {
        when(keyReader.readPrivateKey(any(Reader.class))).thenReturn(key);

        StringPrivateKeyProvider keyProvider = new StringPrivateKeyProvider(keyReader, KEY_VALUE, KEY_ID);

        PrivateKey actualKey = keyProvider.getKey(ValidatedKeyId.validate(KEY_ID));

        assertThat(actualKey, is(key));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldNotRetrieveADifferentKeyId() throws Exception {
        when(keyReader.readPrivateKey(any(Reader.class))).thenReturn(key);

        StringPrivateKeyProvider keyProvider = new StringPrivateKeyProvider(keyReader, KEY_VALUE, KEY_ID);

        keyProvider.getKey(ValidatedKeyId.validate("not-the-key-id"));
    }
}