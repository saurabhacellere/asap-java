package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.SecurityProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.util.Base64;

import static com.atlassian.asap.api.AlgorithmType.ECDSA;
import static com.atlassian.asap.api.AlgorithmType.RSA;
import static com.atlassian.asap.core.keys.DataUriKeyReader.DATA_URI_PEM_HEADER;
import static com.atlassian.asap.core.keys.DataUriKeyReader.DATA_URI_PKCS8_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DataUriKeyReaderTest {
    private static final Provider PROVIDER = SecurityProvider.getProvider();

    private static final String PKCS_KEY_PREFIX = DATA_URI_PKCS8_HEADER + "kid=identifier;base64,";
    private static final String PEM_KEY_PREFIX = DATA_URI_PEM_HEADER + "kid=identifier;base64,";

    @Test
    public void shouldReadRsaPrivateKey() throws Exception {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance(RSA.algorithmName(), PROVIDER);
        PrivateKey privateKey = rsaGenerator.generateKeyPair().getPrivate();
        String privateKeyData = Base64.getEncoder().encodeToString(privateKey.getEncoded());

        PrivateKey decodedPrivateKey = new DataUriKeyReader(PROVIDER).readPrivateKey(new StringReader(PKCS_KEY_PREFIX + privateKeyData));

        assertThat(decodedPrivateKey.getFormat(), is(privateKey.getFormat()));
        assertThat(decodedPrivateKey.getEncoded(), is(privateKey.getEncoded()));
        assertThat(decodedPrivateKey.getAlgorithm(), is(privateKey.getAlgorithm()));
    }

    @Test
    public void shouldReadRsaPublicKey() throws Exception {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance(RSA.algorithmName(), PROVIDER);
        PublicKey publicKey = rsaGenerator.generateKeyPair().getPublic();
        String publicKeyData = Base64.getEncoder().encodeToString(convertToPemData(publicKey));

        PublicKey decodedPublicKey = new DataUriKeyReader(PROVIDER).readPublicKey(new StringReader(PEM_KEY_PREFIX + publicKeyData));

        assertThat(decodedPublicKey.getFormat(), is(publicKey.getFormat()));
        assertThat(decodedPublicKey.getEncoded(), is(publicKey.getEncoded()));
        assertThat(decodedPublicKey.getAlgorithm(), is(publicKey.getAlgorithm()));
    }

    @Test
    public void shouldReadEcdsaPrivateKey() throws Exception {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance(ECDSA.algorithmName(), PROVIDER);
        PrivateKey privateKey = rsaGenerator.generateKeyPair().getPrivate();
        String privateKeyData = Base64.getEncoder().encodeToString(privateKey.getEncoded());

        PrivateKey decodedPrivateKey = new DataUriKeyReader(PROVIDER).readPrivateKey(new StringReader(PKCS_KEY_PREFIX + privateKeyData));

        assertThat(decodedPrivateKey.getFormat(), is(privateKey.getFormat()));
        assertThat(decodedPrivateKey.getEncoded(), is(privateKey.getEncoded()));
        assertThat(decodedPrivateKey.getAlgorithm(), is(privateKey.getAlgorithm()));
    }

    @Test
    public void shouldReadEcdsaPublicKey() throws Exception {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance(ECDSA.algorithmName(), PROVIDER);
        PublicKey publicKey = rsaGenerator.generateKeyPair().getPublic();
        String publicKeyData = Base64.getEncoder().encodeToString(convertToPemData(publicKey));

        PublicKey decodedPublicKey = new DataUriKeyReader(PROVIDER).readPublicKey(new StringReader(PEM_KEY_PREFIX + publicKeyData));

        assertThat(decodedPublicKey.getFormat(), is(publicKey.getFormat()));
        assertThat(decodedPublicKey.getEncoded(), is(publicKey.getEncoded()));
        assertThat(decodedPublicKey.getAlgorithm(), is(ECDSA.name()));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldThrowExceptionForUnsupportedPrivateKeyDataUri() throws Exception {
        new DataUriKeyReader(PROVIDER).readPrivateKey(new StringReader("data:image/gif;base64,R0lGODlhyAAiALMsGLHJgSDYGFTFafas=="));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldThrowExceptionForUnsupportedPublicKeyDataUri() throws Exception {
        new DataUriKeyReader(PROVIDER).readPublicKey(new StringReader("data:image/gif;base64,R0lGODlhyAAiALMsGLHJgSDYGFTFafas=="));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldThrowExceptionForBadPrivateKeyData() throws Exception {
        new DataUriKeyReader(PROVIDER).readPrivateKey(new StringReader(DATA_URI_PKCS8_HEADER + "R0lGODlhyAAiALMsGLHJgSDYGFTFafas=="));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldThrowExceptionForBadPublicKeyData() throws Exception {
        new DataUriKeyReader(PROVIDER).readPublicKey(new StringReader(DATA_URI_PEM_HEADER + "R0lGODlhyAAiALMsGLHJgSDYGFTFafas=="));
    }

    private byte[] convertToPemData(PublicKey publicKey) throws IOException {
        StringWriter stringWriter = new StringWriter();
        JcaPEMWriter pemWriter = new JcaPEMWriter(stringWriter);
        pemWriter.writeObject(publicKey);
        pemWriter.close();
        return stringWriter.toString().getBytes(StandardCharsets.US_ASCII);
    }
}
